package example.com.camerapixels;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ShowResultsActivity extends AppCompatActivity {
    public static String PIXEL_VALUES_KEY = "PIXEL_VALUES_KEY";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_results);
        TextView result = findViewById(R.id.result);
        Intent intent = getIntent();
        if(intent != null) {
            ArrayList<Integer> centralPixelValues = intent.getIntegerArrayListExtra(PIXEL_VALUES_KEY);
            if(centralPixelValues != null && centralPixelValues.size() > 0){
                result.setText("Central Pixel Values: " + "\n"
                        + "R: " + centralPixelValues.get(0) + " "
                        + "G: " + centralPixelValues.get(1) + " "
                        + "B: " + centralPixelValues.get(2));
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
